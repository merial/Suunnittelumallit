package com.mycompany.kksuunnittelu;

import javafx.stage.Stage;
import junit.framework.TestCase;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.junit.Test;
import static org.loadui.testfx.Assertions.verifyThat;
import org.loadui.testfx.GuiTest;
import static org.loadui.testfx.controls.Commons.hasText;

/**
 *
 * @author Meri Alho
 */
public class MainAppTest extends GuiTest {
    

    @Override
    protected Parent getRootNode() {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(MainAppTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return parent;
    }
    
    @Test 
    public void testAction() {
     
        Button one = find("#button");
        verifyThat(one, hasText("Click Me!"));
        
        TextField firstname = find("#text");
        firstname.setText("bennet");
        verifyThat("#text", hasText("bennet"));
        
        TextField lastname = find("#text2");
        lastname.setText("jones");
        verifyThat("#text2", hasText("jones"));

        click(one);

        verifyThat("#label", hasText("bennet jones"));
    }
}
