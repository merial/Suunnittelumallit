/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15;

/**
 *
 * @author Meri Alho
 */
public class Charizard implements Pokemon_IF{

    private int hp = 200;
    private static Charizard INSTANCE = null;
    
    private Charizard(){
        
    }
    
    public static synchronized Charizard getInstance(){
        if(INSTANCE == null){
            INSTANCE = new Charizard();
        }
        return INSTANCE;
    }

    @Override
    public void saySomething() {
        System.out.println("I am Charizard!");
    }

    @Override
    public void fight() {
        System.out.println("Charizard wants to fight you! Charizard lets all hell break loose in a form of a firestorm. Watch out! It's super effective!");
    }

    @Override
    public int getHp() {
        return hp;
    }

    @Override
    public void setHp(int hp) {
        this.hp = hp;
    }

    @Override
    public void heal() {
        hp+=50;
    }


    
    
    
}
