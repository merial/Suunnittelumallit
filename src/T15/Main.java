/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15;

import java.util.ArrayList;

/**
 *
 * @author Meri
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Pokemon_IF charmander = Charmander.getInstance();
        Pokemon_IF charmeleon = Charmeleon.getInstance();
        Pokemon_IF charizard = Charizard.getInstance();
        Pokemon_IF alfred = PokemonAlfredJKwak.getInstance();
        
        ArrayList<Pokemon_IF> pokemonit = new ArrayList<Pokemon_IF>();
        pokemonit.add(charmander);
        pokemonit.add(charmeleon);
        pokemonit.add(charizard);
        pokemonit.add(alfred);
        
        for (Pokemon_IF p : pokemonit){
            p.saySomething();
            System.out.println("Starting HP: " + p.getHp());
            System.out.print("Let's fight! "); p.fight();
            System.out.println("HP after fight " + p.getHp());
            p.heal();
            System.out.println("HP after healing: " + p.getHp());
            System.out.println("_________________________");
                    
        }
        
        
        
    }
    
}
