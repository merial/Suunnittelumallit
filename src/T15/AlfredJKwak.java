/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15;

/**
 *
 * @author Meri
 */
public class AlfredJKwak {

    private int feeling = 6;

    
    public String introduction(){
        return "Hello I am Alfred Jodocus Kwak!";
    }
    
    public String splash(){
        feeling-=2;
        return "SPLASH!!!";
    }
    
    public void setFeeling(int feels){
        feeling = feels;
    }
    public int getFeeling(){
        return feeling;
    }
    
    public String eat(String food){
        feeling+=3;
        return "Kylläpäs " + food +  " maistuukin hyvältä!";
    }
    
    
}
