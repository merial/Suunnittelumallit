/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15;

/**
 *
 * @author Meri
 */
public class PokemonAlfredJKwak implements Pokemon_IF {

    private static PokemonAlfredJKwak INSTANCE = null;
    AlfredJKwak alfred;
    private int hp;
    
    private PokemonAlfredJKwak(AlfredJKwak alf){
        alfred = alf;
    }
    
    public static synchronized PokemonAlfredJKwak getInstance(){
        if(INSTANCE == null){
            AlfredJKwak ajk = new AlfredJKwak();
            INSTANCE = new PokemonAlfredJKwak(ajk);
        }
        return INSTANCE;
    }
    
    @Override
    public void saySomething() {
        System.out.println(alfred.introduction());
    }

    @Override
    public void fight() {
        System.out.println(alfred.splash());
    }

    @Override
    public int getHp() {
        hp = alfred.getFeeling()*20;
        return hp;
    }

    @Override
    public void setHp(int hp) {
        alfred.setFeeling(hp/20);
    }

    @Override
    public void heal() {
        alfred.eat("");
    }
    
}
