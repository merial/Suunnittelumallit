/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15;

/**
 *
 * @author Meri Alho
 */
public class Charmeleon implements Pokemon_IF {
    
    private int hp = 150;
    private static Charmeleon INSTANCE = null;
    
    private Charmeleon(){
        
    }
    
    public static synchronized Charmeleon getInstance(){
        if(INSTANCE == null){
            INSTANCE = new Charmeleon();
        }
        return INSTANCE;
    }

    @Override
    public void saySomething() {
        System.out.println("I am Charmeleon!");
        
    }

    @Override
    public void fight() {
        System.out.println("Charmeleon wants to fight you! Charmeleon coughs up a few fireballs. It's moderately effective. Nice one.");
        hp-=30;
    }


    @Override
    public int getHp() {
        return hp;
    }

    @Override
    public void setHp(int hp) {
        this.hp = hp;
    }

    @Override
    public void heal() {
        hp+=50;
    }

    
    
    
}
