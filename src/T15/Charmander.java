/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15;


/**
 *
 * @author Meri Alho
 */
public class Charmander implements Pokemon_IF {
    
    private static Charmander INSTANCE = null;
    private int hp = 100;
    
    private Charmander(){
        
    }
    
    public static synchronized Charmander getInstance(){
        if(INSTANCE == null){
            INSTANCE = new Charmander();
        }
        return INSTANCE;
    }

    @Override
    public void saySomething() {
        System.out.println("I am Charmander!");
    }

    @Override
    public void fight() {
        System.out.println("Charmander wants to fight you! Charmander lights a match. It's actually quite pathetic but it's still effective enough.");
        hp-=30;
    }


    @Override
    public int getHp() {
        return hp;
    }

    @Override
    public void setHp(int hp) {
        this.hp = hp;
    }

    @Override
    public void heal() {
        hp+=50;
    }



    
    
}
