/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T21;

import java.util.ListIterator;

/**
 *
 * @author Meri
 */
public class Iterating1 implements Runnable {

    Collection collection = Collection.getInstance();
    public Thread t;
    int iterNum;
    
    public Iterating1(int iterNum){
        this.iterNum = iterNum;
    }
    
    @Override
    public void run() {
        
        for(int i: collection.arr){
            System.out.println("Iterator " + iterNum + ": " + i);
        }

    }
    
    
    public void start(){
        System.out.println("Starting thread " + iterNum);
        if (t == null) {
            t = new Thread (this);
            t.start ();
        }
    }
    
}
