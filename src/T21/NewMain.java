/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T21;

import java.util.Arrays;
import java.util.ListIterator;

/**
 *
 * @author Meri
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Collection coll = Collection.getInstance();
        //ListIterator<Integer> iterator = coll.arr.listIterator();
        Iterating1 it1 = new Iterating1(1);
        Iterating2 it2 = new Iterating2(2);
        Iterating2 it3 = new Iterating2(3);
        
        
        it1.start();
        coll.arr.add(666);
        coll.arr.remove(0);
        it2.start();
        it3.start();
        coll.arr.add(777);
        //coll.arr.removeAll(coll.arr);
        coll.arr.addAll(Arrays.asList(11, 22, 33, 44, 55, 66, 77, 88, 99, 111, 222, 333, 444));
        
    }
    
}
