/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T21;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Meri
 */
public class Collection {
    
    private static Collection INSTANCE = null;
    ArrayList<Integer> arr = new ArrayList<Integer>();
    
    private Collection(){
        arr.addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24));
    }
    
    public static synchronized Collection getInstance(){
        if (INSTANCE == null){
            INSTANCE = new Collection();
        }
        return INSTANCE;
    }
    
    
}
