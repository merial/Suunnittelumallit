/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T20;

/**
 *
 * @author Meri
 */
public class Client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StoreKeeper keeper = new StoreKeeper();
        RawMaterialGoods rawgoods = keeper.getRawMaterialGoods();
        PackingMaterialGoods packingmaterial = keeper.getPackingMaterialGoods();
        FinishedGoods finished = keeper.getFinishedGoods();
        
        keeper.getItems(rawgoods);
        keeper.getItems(packingmaterial);
        keeper.getItems(finished);

        
    }
    
}
