/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6;

/**
 *
 * @author Meri
 */
public class Juusto extends Pizzadecorator {

    public Juusto(Pizza toDecorate) {
        super(toDecorate);
    }

    private void addJuusto(){
        System.out.print("juusto, ");
    }
    @Override
    public String make() {
        addJuusto();
        return super.make();
    }

    public int price(){
        //System.out.print("2 ");
        return 2;
    }
    

    
}
