/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6;

/**
 *
 * @author Meri
 */
public class Mozzarella extends Pizzadecorator {

    public Mozzarella(Pizza toDecorate) {
        super(toDecorate);
    }

    private void addMozzarella(){
        System.out.print("mozzarella, " );
    }
    
    @Override
    public String make() {
        addMozzarella();
        return super.make();
    }

    public int price(){
        //System.out.println("2 ");
        return 2;
    }
    
    
}
