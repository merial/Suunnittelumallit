/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6;

/**
 *
 * @author Meri
 */
public class Ananas extends Pizzadecorator{
    
    public Ananas(Pizza toDecorate) {
        super(toDecorate);
    }

    private void addAnanas(){
        System.out.print("ananas, " );
    }
    
    @Override
    public String make() {
        addAnanas();
        return super.make();
    }

    public int price(){
        //System.out.println("2 ");
        return 1;
    }
    
    
}
