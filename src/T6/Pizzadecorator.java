/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6;

/**
 *
 * @author Meri
 */
public abstract class Pizzadecorator implements Pizza{
    
    protected Pizza toDecorate;
    
    public Pizzadecorator(Pizza toDecorate){
        this.toDecorate = toDecorate;
    }
    
    public String make(){
        return toDecorate.make(); //delegointi 
    }
    
    /*public int getPrice(){
        return toDecorate.getPrice(); //delegointi
    }*/
    
    public int getPrice() {
        
        return toDecorate.getPrice()+price();
    }
    public abstract int price();
}
