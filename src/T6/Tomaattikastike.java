/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6;

/**
 *
 * @author Meri
 */
public class Tomaattikastike extends Pizzadecorator {

    public Tomaattikastike(Pizza toDecorate) {
        super(toDecorate);
    }
    
    private void addKastike(){
        System.out.print("tomaattikastike, ");
    }

    @Override
    public String make() {
        addKastike();
        return super.make();
    }

    public int price(){
        //System.out.print("1 ");
        return 1;
    }
    
    
}
