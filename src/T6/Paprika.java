/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6;

/**
 *
 * @author Meri
 */
public class Paprika extends Pizzadecorator{
    
    public Paprika(Pizza toDecorate) {
        super(toDecorate);
    }

    private void addPaprika(){
        System.out.print("paprika, " );
    }
    
    @Override
    public String make() {
        addPaprika();
        return super.make();
    }

    public int price(){
        //System.out.println("2 ");
        return 2;
    }
    
}
