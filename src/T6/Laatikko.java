/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6;

/**
 *
 * @author Meri
 */
public class Laatikko  extends Pizzadecorator {

    public Laatikko(Pizza toDecorate) {
        super(toDecorate);
    }
    
    @Override
    public String make(){
        fillPizza();
        return super.make(); //Abstraktin luokan delegointimetodin kutsu
    }
    
    private void fillPizza(){
        System.out.println("Täytteet: ");
    }
    
    public int price(){
        return 0;
    }
    
    @Override
    public int getPrice(){
        price();
        return super.getPrice();
    }
}
