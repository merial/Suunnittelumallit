/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6;

/**
 *
 * @author Meri
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Pizza decorated = new Laatikko(new Juusto(new Mozzarella(new Sipuli(new Tomaattikastike(new Pohja())))));
        System.out.println(decorated.make() + " \nHinta " + decorated.getPrice() + " €");
        
        decorated = new Laatikko(new Mozzarella(new Paprika(new Sipuli(new Ananas(new Tomaattikastike(new Pohja()))))));
        System.out.println(decorated.make() + " \nHinta " + decorated.getPrice() + " €");
        
        decorated = new Laatikko(new Juusto(new Ananas(new Ananas(new Ananas(new Paprika(new Ananas(new Ananas(new Ananas( new Ananas(new Ananas(new Pohja())))))))))));
        System.out.println(decorated.make() + " \nHinta " + decorated.getPrice() + " €");
        
    }
    
}
