/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2;

/**
 *
 * @author Meri Alho
 */
public interface VaatetusTehdasIF {
    
    Object makeFarkut();
    Object makeKenka();
    Object makeLippis();
    Object makePaita();
    
}
