/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2;

/**
 *
 * @author Meri Alho
 */
public class AdiTehdas implements VaatetusTehdasIF {

    @Override
    public Object makeFarkut() {
        return new AdiFarkut();
    }

    @Override
    public Object makeKenka() {
        return new AdiKenka();
    }

    @Override
    public Object makeLippis() {
       return new AdiLippis();
    }

    @Override
    public Object makePaita() {
        return new AdiPaita();
    }
    
}
