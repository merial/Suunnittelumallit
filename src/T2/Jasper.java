/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Meri Alho
 */
public class Jasper {
    
    public void pueJasper(){
        Class c = null;
        VaatetusTehdasIF tehdas = null;
        
        Properties properties = new Properties();
        
        try{
            properties.load(new FileInputStream("C:\\Users\\Meri\\Documents\\NetBeansProjects\\Suunnittelumallit/srcT2/tehdas.properties"));
        } catch (IOException e){
            e.printStackTrace();
        }        
        try {
            c = Class.forName(properties.getProperty("tehdas"));
            tehdas = (VaatetusTehdasIF)c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        System.out.println("Hei olen Jasper! Minulla on päälläni: " + tehdas.makeFarkut().toString()+ ", " + tehdas.makeKenka().toString() + ", " 
                + tehdas.makeLippis().toString() + " ja "+ tehdas.makePaita().toString());
    }
    
  
}
