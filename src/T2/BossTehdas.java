/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2;

/**
 *
 * @author Meri Alho
 */
public class BossTehdas implements VaatetusTehdasIF {

    @Override
    public Object makeFarkut() {
        return new BossFarkut();
    }

    @Override
    public Object makeKenka() {
        return new BossKenka();
    }

    @Override
    public Object makeLippis() {
        return new BossLippis();
    }

    @Override
    public Object makePaita() {
        return new BossPaita();
    }
    
}
