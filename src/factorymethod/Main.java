package factorymethod;

public class Main {

    public static void main(String[] args) {
        
        Otustehdas tehdas = new Otustehdas();
        
        AterioivaOtus otus1 = tehdas.getOtus();
        AterioivaOtus otus2 = tehdas.getOtus();
        AterioivaOtus otus3 = tehdas.getOtus();
        
        otus1.aterioi();
        otus2.aterioi();
        otus3.aterioi();
        
    }
}
