/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author Meri Alho
 */
public class Otustehdas {
    
    public AterioivaOtus getOtus(){
        
        double arvo = Math.random();
        if(arvo<0.33){
            return new Opettaja();
        }
        else if (arvo>0.33 && arvo<0.66){
            return new Opiskelija();
        }
        else {
            return new Tyontekija();
        }
        
    }
}
