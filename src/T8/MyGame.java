package T8;

import java.util.ArrayList;

/**
 *
 * @author Meri Alho
 */
public class MyGame extends Game{

    public int points=0, dice1, dice2, diceSum, playerInTurn;
    ArrayList<Player> playerList = new ArrayList<Player>(); 
    Player player;
    
    @Override
    void initializeGame() {
        System.out.println("Initializing play...");
        createPlayers(playersCount);
        
    }

    @Override
    void makePlay(int playerNumber) {
        playerInTurn = playerNumber;
        System.out.println("Player " + (playerNumber+1) + " playing!");
        rollDice(playerNumber);
        
        try{ 
            Thread.sleep(990);
        } catch(Exception e){
        }
    }

    @Override
    boolean endOfGame() {
        if (playerList.get(playerInTurn).getPoints() >= 40){
            return true;
        } else {
            return false;
        }
    }

    @Override
    void printWinner() {
        System.out.println("The winner is player: " + (playerInTurn+1) + " with " + playerList.get(playerInTurn).getPoints() + " points!");
    }
    
    private void rollDice(int player){
        dice1 = (int) Math.floor(Math.random() * 6)+1;
        dice2 = (int) Math.floor(Math.random() * 6)+1;
        diceSum = dice1+dice2;
        System.out.println("Player " + (player+1) + " rolled " + dice1 + " and " + dice2);
        points = playerList.get(player).getPoints() + diceSum;
        playerList.get(player).setPoints(points);
        System.out.println("Player " + (player+1) + " has a total of " + playerList.get(player).points + " points.");
        System.out.println("........................");
        
    }
    
    private void createPlayers(int playersAmount){
        System.out.println("Creating players...");
        
        for(int i=0; i<playersAmount; i++){
            player = new Player(i);
            playerList.add(player);
            System.out.println("Player " + (i+1) + " created...");
            
        }
        System.out.println("__________________");
    }
    
}
