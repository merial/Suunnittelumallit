/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9;

/**
 *
 * @author Meri Alho
 */
public class MergeSort implements SortingIF{

    static long laskuri;
    int i, j, k, apu, pienin;
    
    @Override
    public void sort(int[] a, int MAX) {
        mergeSort(a, 0, MAX-1);
        
        System.out.println("\nJärjestetty aineisto:\n");
        for (i=0;i<MAX;i++) {
            
            System.out.print(a[i]+" ");
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println();
        }
        System.out.println("\n\nVertailun tulos: " + laskuri);
        
    }
    
    
    private static void merge(int t[], int p, int q, int r) {
        int MAX = r+1;
        int[] tau = new int[MAX];
        //i osoittaa 1. osataulukkoa, j osoittaa 2. osataulukkoa
        // k osoittaa aputaulukkoa, johon yhdiste kirjoitetaan.
        int i=p, j=q+1, k=0;
        laskuri+=2;
        while(i<q+1 && j<r+1) {
            laskuri++;
                if (t[i]<t[j]) {
                        tau[k++]=t[i++];
                }
                else {
                        tau[k++]=t[j++];
                }
                laskuri+=2;
        }
        //toinen osataulukko käsitelty, siirretään toisen käsittelemättömät
        laskuri++;
        while (i<q+1){  
                tau[k++]=t[i++];
                laskuri++;
        }
        laskuri++;
        while (j<r+1){
            tau[k++]=t[j++];
            laskuri++;
        }
        //siirretään yhdiste alkuperäiseen taulukkoon
        laskuri++;
        for (i=0;i<k;i++) {
                t[p+i]=tau[i];
                laskuri++;
        }
    }
    
    private static void mergeSort(int t[],  int alku,  int loppu) {
        
        int ositus;
        long la, ll, lt;
        laskuri++;
        if (alku<loppu) { //onko väh. 2 alkiota, että voidaan suorittaa ositus

                la=alku; ll=loppu;
                lt=(la+ll)/2;
                ositus=(int)lt;
                mergeSort(t, alku, ositus);//lajitellaan taulukon alkupää
                mergeSort(t, ositus+1, loppu);//lajitellaan taulukon loppupää
                merge(t, alku, ositus, loppu);//yhdistetään lajitellut osataulukot
        }

    }
    
}
