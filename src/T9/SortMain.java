/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9;

import java.util.Random;

/**
 *
 * @author Meri Alho
 */
public class SortMain {

    /**
     * @param args the command line arguments
     */
    final static int MAX=3000;
    
    public static void main(String[] args) {
        
        int[] sortMaterial;
        Sorter sorter;
        
        sorter = new Sorter(new QuickSort());
        sortMaterial = sorter.generateNumberArray(MAX);
        sorter.startSort(sortMaterial, MAX);
        
        sorter = new Sorter(new MergeSort());
        sortMaterial = sorter.generateNumberArray(MAX);
        sorter.startSort(sortMaterial, MAX);
        
        sorter = new Sorter(new SelectSort());
        sortMaterial = sorter.generateNumberArray(MAX);
        sorter.startSort(sortMaterial, MAX);
    
    }
    
}
