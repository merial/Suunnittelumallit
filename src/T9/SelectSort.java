/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9;


/**
 *
 * @author Meri Alho
 */
public class SelectSort implements SortingIF {
    
    static long laskuri;
    int i, j, k, apu, pienin;
    
    public void sort(int[] taul, int MAX){
        laskuri++;
        for (i=0;i<MAX;i++) {
            pienin=i;
            laskuri++;
            for (j=i+1;j<MAX;j++) {
                laskuri++;
                /* löytyykö taulukon loppupäästä pienempää alkiota? */
                if (taul[j] < taul[pienin]) {
                    pienin=j;
                }
                laskuri++;
            }
            laskuri++;
            if (pienin != i) {
                /* jos löytyi suoritetaan alkioiden vaihto */
                apu=taul[pienin];
                taul[pienin]=taul[i];
                taul[i]=apu;
            }
            laskuri++;
        }
        System.out.println();
        System.out.println("\nJärjestetty aineisto:\n");
        for (i=0;i<MAX;i++) {
            System.out.print(taul[i]+" ");
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println();
        }
        System.out.println("\n\nVertailun tulos: " + laskuri);
    }

}
