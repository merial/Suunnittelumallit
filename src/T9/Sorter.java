/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9;

import java.util.Random;

/**
 *
 * @author Meri Alho
 */
public class Sorter {
    
    private SortingIF sorting;
            
    public Sorter(SortingIF sorting){
        this.sorting = sorting;
    }
    
    public int[] generateNumberArray(int MAX){
        int[] a= new int[MAX];
        int i;
        Random r = new Random(); //luodaan satunnaislukugeneraattori
        System.out.println("\nGeneroidaan syöttöaineisto: ");
        for (i=0;i<MAX;i++) {
            a[i] = r.nextInt(1000); //generoidaan luvut
            System.out.print(a[i]+" ");
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println();
        }
        return a;
    }
    
    public void startSort(int arr[], int max){
        sorting.sort(arr, max);
    }
    
    public void setStrategy(SortingIF sorting){
        this.sorting = sorting;
    }
}
