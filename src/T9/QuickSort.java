/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9;

/**
 *
 * @author Meri Alho
 */
public class QuickSort implements SortingIF{
    static long laskuri;
    int i, j, k, apu, pienin;

    @Override
    public void sort(int[] arr, int max) {
        int values = max-1;
        quickSort(arr, 0, values);

        System.out.println("\nJärjestetty aineisto:\n");
        for (int i = 0; i < values; i++) {
            System.out.print(arr[i] + " ");
            if (i > 0 && i % 40 == 0) { // rivinvaihto
                System.out.println();
            }
        }
        System.out.println("\n\nVertailujen lukumäärä: " + laskuri);
    }

    private void quickSort(int[] table, int lo0, int hi0) {
        int lo = lo0;
        int hi = hi0;
        int mid, swap;

        mid = table[ (lo0 + hi0) / 2];
        laskuri++;
        while (lo <= hi) {
            laskuri++;
            while (table[lo] < mid) {
                ++lo;
                laskuri++;
            }
               laskuri++;
            while (table[hi] > mid) {
                --hi;
                laskuri++;
            }
            laskuri++;
            if (lo <= hi) {
                swap = table[lo];
                table[lo] = table[hi];
                ++lo;
                table[hi] = swap;
                --hi;
            }
            laskuri++;
        }

        laskuri++;
        if (lo0 < hi) {
            quickSort(table, lo0, hi);
        }
        laskuri++;
        if (lo < hi0) {
            quickSort(table, lo, hi0);
        }
    }
}

