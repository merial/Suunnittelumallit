/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T10;

/**
 *
 * @author Meri
 */
public abstract class Handler {
    
    Handler successor;
    
    public Handler(Handler successor){
        this.successor = successor;
    }

    /*public void handle() {
        if (successor != null) {
            successor.handle();
        }
    }*/
    abstract public void processRequest(Request request);

}
