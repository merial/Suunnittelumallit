/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T10;

/**
 *
 * @author Meri
 */
public class CloseManager extends Handler{

    private final double allowableMax = 2.00;
    public CloseManager(Handler s) {
        super(s);
    }
    

    @Override
    public void processRequest(Request request) {
        if(request.getAmount() <= allowableMax){
            System.out.println("The close manager will handle the raise request.");
        } else {
            successor.processRequest(request);
        }
    }
    
}
