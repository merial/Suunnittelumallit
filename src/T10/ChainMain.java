/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T10;

import java.util.Scanner;

/**
 *
 * @author Meri
 */
public class ChainMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        CEO ceo = new CEO(null);
        UnitManager um = new UnitManager(ceo);
        CloseManager cm = new CloseManager(um);

        while (true) {
            System.out.println("Enter the percentage of your raise request.");
            System.out.print(">");
            double d = Double.parseDouble(scan.nextLine());
            cm.processRequest(new Request(d, null));
            
        }
        
    }
    
}
