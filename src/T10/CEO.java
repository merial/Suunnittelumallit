/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T10;

/**
 *
 * @author Meri
 */
public class CEO extends Handler {

    private final double allowableMin = 5.00; 
    
    public CEO(Handler successor) {
        super(successor);
    }

    @Override
    public void processRequest(Request request) {
        if(request.getAmount() > allowableMin){
            System.out.println("The CEO will handle the raise request.");
        } else {
            successor.processRequest(request);
        }
    }
    
    
    
}
