/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T10;

/**
 *
 * @author Meri
 */
public class UnitManager extends Handler {

    private final double allowableMax = 5.00;
    private final double allowableMin = 2.00;
    
    public UnitManager(Handler s) {
        super(s);
    }

    @Override
    public void processRequest(Request request) {
        if(request.getAmount() >= allowableMin && request.getAmount() <= allowableMax){
            System.out.println("The unit manager will handle the raise request.");
        } else {
            successor.processRequest(request);
        }
    }
    
}
