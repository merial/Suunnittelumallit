/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T18;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Meri Alho
 */
public class Tuntiviisari implements Cloneable {

    private String tunti;
    
    public String getTunti(){
        DateFormat dateFormat = new SimpleDateFormat("HH");
        Calendar cal = Calendar.getInstance();
        tunti = dateFormat.format(cal.getTime());
        return tunti;
    }
    
    public Tuntiviisari clone(){
        Tuntiviisari t = null;
        
        try{
            t = (Tuntiviisari)super.clone();
        } catch(CloneNotSupportedException e){
            e.printStackTrace();
        }
        return t;
    }
    
}
