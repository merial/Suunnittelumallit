/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T18;

/**
 *
 * @author Meri
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Kello klo = new Kello();
        
        Kello uusiklo = klo.clone();
        
        while(true){
            try{
                System.out.print("Original clock..");klo.show();
                System.out.print("Clone clock....."); uusiklo.show();
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        
    }
    
}
