/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T18;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Meri Alho
 */
public class Sekuntiviisari implements Cloneable{

    private String sekunti;
    
    public String getSekunti(){
        DateFormat dateFormat = new SimpleDateFormat("ss");
        Calendar cal = Calendar.getInstance();
        sekunti = dateFormat.format(cal.getTime());
        return sekunti;
    }

    public Sekuntiviisari clone(){
        Sekuntiviisari s = null;
        
        try {
            s = (Sekuntiviisari)super.clone();
        } catch(CloneNotSupportedException e){
            e.printStackTrace();
        }
        return s;
    }
    
}
