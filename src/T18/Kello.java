/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T18;

/**
 *
 * @author Meri Alho
 */
public class Kello implements Cloneable{
    private Minuuttiviisari min;
    private Sekuntiviisari sek;
    private Tuntiviisari h;
    
    public Kello(){
        min = new Minuuttiviisari();
        sek = new Sekuntiviisari();
        h = new Tuntiviisari();
    }
    
    public void show(){
        System.out.println(h.getTunti() + ":" + min.getMinuutti() + ":" + sek.getSekunti());
    }
    
    @Override
    public Kello clone(){
        Kello klo = null;
        try{
            klo = (Kello) super.clone();
            klo.h = (Tuntiviisari)h.clone();
            klo.min = (Minuuttiviisari)min.clone();
            klo.sek = (Sekuntiviisari)sek.clone();
            
        } catch(CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return klo;
    }
    
    
    
}
