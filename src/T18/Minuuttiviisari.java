/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T18;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Meri Alho
 */
public class Minuuttiviisari implements Cloneable {

    private String minuutti;
    
    public String getMinuutti(){
        DateFormat dateFormat = new SimpleDateFormat("mm");
        Calendar cal = Calendar.getInstance();
        minuutti = dateFormat.format(cal.getTime());
        return minuutti;
    }
    
    public Minuuttiviisari clone(){
        Minuuttiviisari m = null;
        
        try{
            m = (Minuuttiviisari)super.clone();
        } catch(CloneNotSupportedException e){
            e.printStackTrace();
        }
        return m;
    }
    
}
