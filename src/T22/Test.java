/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T22;

/**
 *
 * @author Meri Alho
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Screen screen = new Screen();
        Command rollUp = new RollUpCommand(screen);
        Command rollDown = new RollDownCommand(screen);
        
        WallButton upbtn = new WallButton(rollUp);
        WallButton downbtn =  new WallButton(rollDown);
        
        upbtn.push();
        downbtn.push();
        downbtn.push();
        upbtn.push();
    }
    
}
