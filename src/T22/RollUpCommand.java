/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T22;

/**
 *
 * @author Meri Alho
 */
public class RollUpCommand implements Command {

    private Screen screen;
    
    public RollUpCommand(Screen screen){
        this.screen = screen;
    }
    
    @Override
    public void execute() {
        screen.screenUp();
    }
    
}
