/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T22;

/**
 *
 * @author Meri Alho
 */
public class RollDownCommand implements Command{
    private Screen screen;
    
    public RollDownCommand(Screen screen){
        this.screen = screen;
    }

    @Override
    public void execute() {
        screen.screenDown();
    }
    
    
}
