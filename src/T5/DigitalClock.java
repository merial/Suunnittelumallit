/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T5;

import T2.VaatetusTehdasIF;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

/**
 *
 * @author Meri Alho
 */
public class DigitalClock {


    public static void main(String[] args){
        
        
        //ClockTimer clock = new ClockTimer();
        
        /*Class c = null;
        ClockTimer timer = null;
        
        Properties properties = new Properties();
        
        try{
            properties.load(new FileInputStream("./src/T5/Kellohaku.properties"));
        } catch (IOException e){
            e.printStackTrace();
        }        
        try {
            c = Class.forName(properties.getProperty("kello"));
            Method method = c.getMethod("getInstance", new Class[0]);
            Object o = method.invoke(c, new Object[0]);
            
            timer = (ClockTimer)c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        
        
        ClockTimer timer = ClockTimer.getInstance();
        
        ClockTimer timer2 = ClockTimer.getInstance();
        
        ClockTimer timer3 = ClockTimer.getInstance();
        
        if (timer == timer2 && timer == timer3 && timer2 == timer3) {
            System.out.println("Sama instanssi käytössä");
            System.out.println("Timer 1: " + timer);
            System.out.println("Timer 2: " + timer2);
            System.out.println("Timer 3: " + timer3);
        }
        
        
        timer.addObserver(new Observer(){

            @Override
            public void update(Observable o, Object arg) {
                System.out.println(arg);
            }
        });
        
        new Thread(timer).start();
    }
    
    
}
