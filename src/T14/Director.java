/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T14;

/**
 *
 * @author Meri
 */
public class Director {
    
    private BurgerBuilder builder;
    
    public void setBurgerBuilder(BurgerBuilder bb){
        builder = bb;
    }
    
    public void constructBurger(){
        builder.createNewBurgerProduct();
        builder.buildBurger();
    }
    
    public Burger getBurger(){
        return builder.getBurger();
    }
    
}
