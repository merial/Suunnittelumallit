/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T14;

/**
 *
 * @author Meri
 */
public class HeseBurger extends BurgerBuilder {

    StringBuilder stringb;

    @Override
    public void buildBurger() {
        stringb = new StringBuilder();
        
        burger.setBun("Hese bun,");
        burger.setCheese(" cheddar,");
        burger.setPatty(" chickpea patty,");
        burger.setSalad(" iceberg lettuce,");
        burger.setSauce(" mayonnaise");
        burger.setTomato(" ");
        
        stringb.append(burger.getBun());
        stringb.append(burger.getCheese());
        stringb.append(burger.getPatty());
        stringb.append(burger.getSalad());
        stringb.append(burger.getSauce());
        stringb.append(burger.getTomato());
        
        burger.setBurger(stringb);
        
        System.out.println("\n"+stringb.toString());
    }
    
    
    
}
