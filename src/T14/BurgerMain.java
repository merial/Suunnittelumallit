/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T14;

/**
 *
 * @author Meri
 */
public class BurgerMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Director dir = new Director();
        BurgerBuilder heseBuilder = new HeseBurger();
        BurgerBuilder mcBuilder = new MCBurger();
        
        dir.setBurgerBuilder(mcBuilder);
        dir.constructBurger();
        Burger mcburger = dir.getBurger();
        //System.out.println(mcburger);
        
        dir.setBurgerBuilder(heseBuilder);
        dir.constructBurger();
        Burger heseburger = dir.getBurger();
        //System.out.println(heseburger);
        
    }
    
}
