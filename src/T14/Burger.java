/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T14;

import java.util.ArrayList;

/**
 *
 * @author Meri
 */
public class Burger {
    
    private String bun;
    private String sauce;
    private String patty;
    private String cheese;
    private String salad;
    private String tomato;
    private ArrayList<String> list;
    private StringBuilder sb;
    
    public void setBurger(StringBuilder sb){
        this.sb = sb;
    }
    
    public void setBurger(ArrayList<String> list){
        this.list = list;
    }
    
    public void setBun(String bun){
        this.bun = bun;
    }

    public String getBun() {
        return bun;
    }

    public String getSauce() {
        return sauce;
    }

    public String getPatty() {
        return patty;
    }

    public String getCheese() {
        return cheese;
    }

    public String getSalad() {
        return salad;
    }

    public String getTomato() {
        return tomato;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public void setPatty(String patty) {
        this.patty = patty;
    }

    public void setCheese(String cheese) {
        this.cheese = cheese;
    }

    public void setSalad(String salad) {
        this.salad = salad;
    }

    public void setTomato(String tomato) {
        this.tomato = tomato;
    }
    
    
}
