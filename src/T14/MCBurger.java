/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T14;

import java.util.ArrayList;

/**
 *
 * @author Meri
 */
public class MCBurger extends BurgerBuilder{

    @Override
    public void buildBurger() {
        ArrayList<String> burgerList = new ArrayList<String>();
        
        burger.setBun("Macdonalds bun,");
        burger.setCheese(" cheddar,");
        burger.setPatty(" meat patty,");
        burger.setSalad(" regular lettuce,");
        burger.setSauce(" ketchup,");
        burger.setTomato(" 2 tomato slices");
        
        burgerList.add(burger.getBun());
        burgerList.add(burger.getCheese());
        burgerList.add(burger.getPatty());
        burgerList.add(burger.getSalad());
        burgerList.add(burger.getSauce());
        burgerList.add(burger.getTomato());
        
        burger.setBurger(burgerList);
        System.out.println("");
        
        for(String x : burgerList){
            System.out.print(x);
        }
        
    }
    
    
}
