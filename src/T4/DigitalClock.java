/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T4;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Meri Alho
 */
public class DigitalClock {

    /*private ClockTimer timer;    
    public DigitalClock(ClockTimer ct){
        timer = ct;
    }*/

    public static void main(String[] args){
        
        ClockTimer ct = new ClockTimer();
        
        ct.addObserver(new Observer(){

            @Override
            public void update(Observable o, Object arg) {
                System.out.println(arg);
            }
        });
        
        new Thread(ct).start();
    }
    
    
    
}
