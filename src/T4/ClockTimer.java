/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T4;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Observable;

/**
 *
 * @author Meri Alho
 */
public class ClockTimer extends Observable implements Runnable {

    String time;
    
    public String getTime(){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        time = dateFormat.format(cal.getTime());
        return time;
    }
        
    public void tick(){
        time = getTime();
        setChanged();
        notifyObservers(this.time);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Error Occurred.");
        }

    }

    @Override
    public void run() {
        while (true){
            
            tick();
        }
    }
    
}
