/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T13;

/**
 *
 * @author Meri
 */
public class BonusVisitor implements CharVisitor_IF {

    @Override
    public void visit(StateCharmander ch) {
        System.out.println("A WILD CHARMANDERVISITOR APPEARS!");
        ch.setHp(ch.getHp(null)+50);
        System.out.println("Charmander gets bonus hp after fighting, now hp is " + ch.getHp(null));
    }

    @Override
    public void visit(StateCharmeleon cm) {
        System.out.println("A WILD CHARMELEONVISITOR APPEARS!");
        cm.setHp(cm.getHp(null)+70);
        System.out.println("Charmeleon gets bonus hp after fight, hp is now " + cm.getHp(null));
    }

    @Override
    public void visit(StateCharizard cz) {
        System.out.println("SUPER RARE WILD CHARIZARDVISITOR APPEARS!");
        cz.setHp(cz.getHp(null)+100);
        System.out.println("Charizard gets bonus hp for being awesome!!! Hp is now " + cz.getHp(null));
    }
    
}
