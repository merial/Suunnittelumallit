/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T13;


/**
 *
 * @author Meri Alho
 */
public class StateCharizard extends CharState{

    private int hp = 200;
    private int counter = 0;
    private static StateCharizard INSTANCE = null;
    private CharVisitor_IF visit = new BonusVisitor();
    private int points = 0;
    
    private StateCharizard(){
        
    }
    
    public static synchronized StateCharizard getInstance(){
        if(INSTANCE == null){
            INSTANCE = new StateCharizard();
        }
        return INSTANCE;
    }

    @Override
    public void saySomething(Character c) {
        System.out.println("I am Charizard!");
    }

    @Override
    public void fight(Character c) {
        System.out.println("Charizard wants to fight you! Charizard lets all hell break loose in a form of a firestorm. Watch out! It's super effective!");
        counter++;
        
        if (counter < 2){
            changeState(c, StateCharizard.getInstance());
        }
        else {
            changeState(c, StateCharmander.getInstance());
            System.out.println("Oh no!!! Something strange happens and Charizard shrinks back to Charmander... Dont know why...");
            counter=0;
            accept(visit);
        }
        
        
    }

    @Override
    public int getHp(Character c) {
        return hp;
    }
    
    @Override
    public void setHp(int newHp){
        this.hp = newHp;
    }

    @Override
    public void accept(CharVisitor_IF visitor){
        visitor.visit(this); 
    }
    
}
