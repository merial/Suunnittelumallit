/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T13;


/**
 *
 * @author Meri
 */
public class Character {
    
    private CharState state;
    
    public Character(){
        state = StateCharmander.getInstance();
    }
    
    protected void changeState(CharState s){
        state = s;
    }
    
    public void talk(){
        state.saySomething(this);
    }
    
    public int healthPoints(){
        return state.getHp(this);
    }
    
    public void useAttack(){
        state.fight(this);
    }
    
    
}
