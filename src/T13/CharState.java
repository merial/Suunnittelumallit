/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T13;


/**
 *
 * @author Meri
 */
abstract class CharState {
    
    void saySomething(Character c){}
    void fight(Character c){}
    void walkForward(Character c){}
    int getHp(Character c){
        return 0;
    }
    
    void setHp(int newHp){}
    
    void changeState(Character c, CharState s){
        c.changeState(s);
    }
    
    void accept(CharVisitor_IF visitor){}
    
}
