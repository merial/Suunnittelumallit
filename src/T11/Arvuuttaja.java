/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T11;

/**
 *
 * @author Meri
 */
public class Arvuuttaja {
    
    private int luku;
    
    public Memento liityPeliin(Asiakas asiakas){
        luku = (int)(Math.random()*10);
        return new Memento(this.luku);
    }
    
    public Object save(){
        return new Memento(this.luku);
    }
    
    public boolean compareToMemento(Object obj, int arvaus){
        Memento memento = (Memento)obj;
        this.luku = memento.getNumber();
        
        if (luku == arvaus){
            System.out.println("Arvasit oikein! Luku oli " + luku);
            return true;
        } else {
            System.out.println("Väärin meni");
            return false;
        }
        
    }
    
    private class Memento{
        
        private final int arvattava;
        
        public Memento(int numero){
            arvattava=numero;
        }
        
        public int getNumber(){
            return arvattava;
        }
    }
}
