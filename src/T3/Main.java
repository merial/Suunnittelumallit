/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T3;

/**
 *
 * @author Meri Alho
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Laiteosa kaikkiosat = new LaiteosaComp();
        
        Laiteosa ram = new RAM();
        Laiteosa ram2 = new RAM();
        
        Laiteosa gpu = new GPU();
        
        Laiteosa cpu = new CPU();
        
        Laiteosa mb = new Motherboard();
        mb.addOsa(ram);
        mb.addOsa(ram2);
        mb.addOsa(gpu);
        mb.addOsa(cpu);
        
        Laiteosa box = new Box();
        box.addOsa(mb);
        
        kaikkiosat.addOsa(box);
        
        System.out.println("Kokonaishinta: " + kaikkiosat.getHinta());
    }
    
}
