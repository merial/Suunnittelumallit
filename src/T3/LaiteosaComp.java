/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T3;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Meri
 */
public class LaiteosaComp implements Laiteosa{
    
    List<Laiteosa> osalista = new ArrayList<Laiteosa>();

    @Override
    public double getHinta() {
        double hinta=0;
        for(Laiteosa o : osalista){
            hinta += o.getHinta();
        }
        return hinta;
    }

    @Override
    public void addOsa(Laiteosa osa) {
        osalista.add(osa);
    }
        
    
}
