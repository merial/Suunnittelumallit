/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T7;

/**
 *
 * @author Meri Alho
 */
public class Play {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Character chara = new Character();
        
        for (int i=0; i<7; i++){
            chara.talk();
            System.out.println("I have " + chara.healthPoints() + " hp.");
            chara.useAttack();
            System.out.println("_____________________________");
        }
        
    }
    
}
