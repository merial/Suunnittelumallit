/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T7;

/**
 *
 * @author Meri Alho
 */
public class StateCharmander extends CharState {
    
    private static StateCharmander INSTANCE = null;
    private int hp = 100;
    
    private StateCharmander(){
        
    }
    
    public static synchronized StateCharmander getInstance(){
        if(INSTANCE == null){
            INSTANCE = new StateCharmander();
        }
        return INSTANCE;
    }

    @Override
    public void saySomething(Character c) {
        
        System.out.println("I am Charmander!");
    }

    @Override
    public void fight(Character c) {
        System.out.println("Charmander wants to fight you! Charmander lights a match. It's actually quite pathetic but it's still effective enough.");
        changeState(c, StateCharmeleon.getInstance());
    }

    @Override
    public int getHp(Character c) {
        return hp;
    }

    
    
}
