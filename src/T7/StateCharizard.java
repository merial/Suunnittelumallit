/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T7;

/**
 *
 * @author Meri Alho
 */
public class StateCharizard extends CharState{

    private int hp = 200;
    private int counter = 0;
    private static StateCharizard INSTANCE = null;
    
    private StateCharizard(){
        
    }
    
    public static synchronized StateCharizard getInstance(){
        if(INSTANCE == null){
            INSTANCE = new StateCharizard();
        }
        return INSTANCE;
    }

    @Override
    public void saySomething(Character c) {
        System.out.println("I am Charizard!");
    }

    @Override
    public void fight(Character c) {
        System.out.println("charizard wants to fight you! Charizard lets all hell break loose in a form of a firestorm. Watch out! It's super effective!");
        counter++;
        
        if (counter < 3){
            changeState(c, StateCharizard.getInstance());
        }
        else {
            changeState(c, StateCharmander.getInstance());
            System.out.println("Oh no!!! Something strange happens and Charizard shrinks back to Charmander... Dont know why...");
            counter=0;
        }
        
    }

    @Override
    public int getHp(Character c) {
        return hp;
    }

    
    
    
}
