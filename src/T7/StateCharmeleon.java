/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T7;

/**
 *
 * @author Meri Alho
 */
public class StateCharmeleon extends CharState {
    
    private int hp = 150;
    private static StateCharmeleon INSTANCE = null;
    
    private StateCharmeleon(){
        
    }
    
    public static synchronized StateCharmeleon getInstance(){
        if(INSTANCE == null){
            INSTANCE = new StateCharmeleon();
        }
        return INSTANCE;
    }

    @Override
    public void saySomething(Character c) {
        System.out.println("I am Charmeleon!");
    }

    @Override
    public void fight(Character c) {
        System.out.println("Charmeleon wants to fight you! Charmeleon coughs up a few fireballs. It's moderately effective. Nice one.");
        changeState(c, StateCharizard.getInstance());
    }

    @Override
    public int getHp(Character c) {
        return hp;
    }
    
    
    
}
